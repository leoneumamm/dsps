function [LF,HF,VLF,LFnu,HFnu,LF_HF,TotalPower,bands] = psdintegral(F,Pxx)

askband = figure('Position',[300,300,200,200],'Menubar','None','resize','off',...
    'color',[0.839,0.91,0.851],'WindowStyle','Modal');
tvlf = uicontrol('Style','Text','String','VLF Band','Position',[70, 178,50,20]...
    ,'backgroundcolor',[0.839,0.91,0.851]);
vlf1 = uicontrol('Style','Edit','String','0.003','Position',[20,150,70,20]);
vlf2 = uicontrol('Style','Edit','String','0.04','Position',[95,150,70,20]);
disp(vlf1)
tlf = uicontrol('Style','Text','String','LF Band','Position',[70, 128,50,20],...
    'backgroundcolor',[0.839,0.91,0.851]);
lf1 = uicontrol('Style','Edit','String','0.04','Position',[20,100,70,20]);
lf2 = uicontrol('Style','Edit','String','0.15','Position',[95,100,70,20]);
thf = uicontrol('Style','Text','String','HF Band','Position',[70, 78,50,20],...
    'backgroundcolor',[0.839,0.91,0.851]);
hf1 = uicontrol('Style','Edit','String','0.15','Position',[20,50,70,20]);
hf2= uicontrol('Style','Edit','String','0.5','Position',[95,50,70,20]);
btband = uicontrol('String','Ok','CallBack','uiresume(gcbf)');
uiwait(askband)
start_vlf = str2double(get(vlf1,'String'));
stop_vlf = str2double(get(vlf2,'String'));
start_lf = str2double(get(lf1,'String'));
stop_lf = str2double(get(lf2,'String'));
start_hf = str2double(get(hf1,'String'));
stop_hf = str2double(get(hf2,'String'));
close(askband)
LF = trapz(F(find(F >= start_lf,1):find(F >= stop_lf,1)),Pxx(find(F>= start_lf,1):find(F >= stop_lf,1)));
HF = trapz(F(find(F >= start_hf,1):find(F >= stop_hf,1)),Pxx(find(F >= start_hf,1):find(F >= stop_hf,1)));
VLF = trapz(F(find(F>=start_vlf,1):find(F >= stop_vlf,1)),Pxx(find(F >=start_vlf,1):find(F >= stop_vlf,1)));
TotalPower = trapz(F(1:find(F >= stop_hf,1)));
LFnu = (LF/(TotalPower - VLF)) *100;
HFnu = (HF/(TotalPower - VLF)) *100;
LF_HF = LF./HF;
bands = [start_vlf,stop_vlf,start_lf,stop_lf,start_hf,stop_hf];
end